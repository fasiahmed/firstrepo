FROM node:6.3.0

RUN mkdir -p /opt/tee
WORKDIR /opt/tee

COPY trials-experience-engine /opt/tee
RUN npm install -g bower
RUN npm install -g grunt-cli
RUN npm install
RUN echo '{"allow_root": true}' > /root/.bowerrc
RUN bower install

RUN  grunt deploy-dev
EXPOSE 3000

CMD ["npm","start"]